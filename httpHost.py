# Host HTTP Python
import http.server
import socketserver

HOST = '0.0.0.0'
PORT = 8000

Handler = http.server.SimpleHTTPRequestHandler

httpd = socketserver.TCPServer((HOST, PORT), Handler)

print("View log at http://%s:%d" % (HOST, PORT))
httpd.serve_forever()
