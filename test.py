import inverse as inv
from time import sleep
from js import js

jnum = 3 # num of joints

lengths = [5] * jnum # length of each part of arm

arm_ = inv.arm(lengths)
#arm.joints = [30] * jnum
js(arm_.lengths, arm_.joints)

arm_.goal = [0, 1]

while(1):
    arm_.up()
