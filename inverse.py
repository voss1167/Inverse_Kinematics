'''
Inverse Kinematics
By Andrew Voss
'''
import math

class arm:

    def __init__(self, length = [0]):
        self.goal = [0, 0] # Goal pos for hand
        self.lengths = length # Length of each part of arm
        self.joints = [180] * len(length) # Init pos of each joint folded up in resting pos
        self.joints[0] = 0
        self.pos = self.goal

    # Change init joint position
    def initPos(self, joint, pos):
        self.joints[joint] = pos

    def up(self):
        self.goal[1] += 1
        self.move();

    def down(self):
        self.goal[1] -= 1
        self.move();

    def forward(self):
        self.goal[0] += 1
        self.move();

    def backwards(self):
        self.goal[0] -= 1
        self.move();

    def move(self):
        if(self.pos != self.goal):
            print("moving")
        else:
            print("stopped")

class segments:

    def __init__(self):
        self.self = self
