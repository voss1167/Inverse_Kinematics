var arm = document.getElementById("arm");
var ctx = arm.getContext("2d");

var origin = [arm.width / 2, arm.height];

async_message();

function degreeToRad(degree){
  var rad = degree * (Math.PI / 180);
  return rad;
}

function getPos(angle, lenght){
  angle = degreeToRad(angle);
  var x = Math.cos(angle) * lenght;
  var y = Math.sin(angle) * lenght;
  return [x, y];
}

function arraySum(array){
  var newArray = 0;
  for(i in array){
    newArray += array[i];
  }
  return newArray;
}

function drawArm(){
  ctx.beginPath();
  ctx.strokeStyle = "black";
  ctx.moveTo(origin[0], origin[1]);
  var scale = arm.width / arraySum(lengths) / 2;
  var end = origin; // end point of lines
  var last = 0;
  for(i in lengths){
    pos = getPos(joints[i] + last, lengths[i]);
    last += joints[i];
    var x = end[0] - (pos[0] * scale);
    var y = end[1] - (pos[1] * scale);
    ctx.lineTo(x, y)
    end = [x, y];
  };
  ctx.clearRect(0, 0, arm.width, arm.height);
  ctx.stroke();
};

function drawGrasper(){
  ctx.beginPath();
  ctx.strokeStyle = "red";
};

function drawShoulder(){
  ctx.beginPath();
  ctx.strokeStyle = "red";
  ctx.moveTo(origin[0], origin[1]);
  ctx.arc(origin[0], origin[1], 2, 0, 2 * Math.PI);
  ctx.closePath();
  ctx.fill();
  ctx.stroke();
};

function draw(){
  drawArm();
  drawShoulder();
};

function async_message(){
  delete lengths;
  delete joints;
  var armData_script = document.getElementById('data');
  var armData_script_parent = armData_script.parentNode;
  armData_script_parent.removeChild(armData_script);
  var script = document.createElement('script');
  script.src = 'armData.js';
  script.id = 'data';
  armData_script_parent.appendChild(script);
  draw();
  setTimeout(function(){async_message()}, 50);
};
