import sys, tty, termios, signal
import inverse as inv

lengths = [10, 20, 15, 5]
arm = inv.kinematics(lengths)

fd = sys.stdin.fileno() # I don't know what this does
old_settings = termios.tcgetattr(fd)

tty.setraw(sys.stdin.fileno()) # this sets the style of the input
while True:
    arm.move();
    ch = sys.stdin.read(1) # this reads one character of input without requiring an enter keypress
    if ch == '*': # pressing the asterisk key kills the process
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings) # this resets the console settings
        break # this ends the loop
    if(ch == "w"):
        print("UP")
    if(ch == "s"):
        print("DOWN")
    if(ch == "a"):
        print("BACKWARDS")
    if(ch == "d"):
        print("FORWARD")
