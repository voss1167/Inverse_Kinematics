# Kinematics

Created by Andrew Voss for BSM Robotics

## Tasks

- [x] Visual
- [ ] Inverse Kinematics
- [ ] Kinematics w/ infinite number of joints
